import react from '@vitejs/plugin-react';
import path from 'path';
import { defineConfig } from 'vite';

// https://vitejs.dev/config/
export default defineConfig({
	resolve: {
		alias: {
			'@': path.resolve(__dirname, './src'),
			'@assets': path.resolve(__dirname, './src/assets'),
			'@components': path.resolve(__dirname, './src/components'),
			'@gql': path.resolve(__dirname, './src/gql'),
			'@generated': path.resolve(__dirname, './src/generated'),
			'@layouts': path.resolve(__dirname, './src/layouts'),
			'@pages': path.resolve(__dirname, './src/pages'),
			'@utils': path.resolve(__dirname, './src/utils'),
			'@styles': path.resolve(__dirname, './src/styles'),
			'@routes': path.resolve(__dirname, './src/routes'),
		},
	},
	plugins: [react()],
	server: {
		watch: {
			usePolling: true,
		},
		host: true,
		port: 5194,
		strictPort: true,
	},
});
