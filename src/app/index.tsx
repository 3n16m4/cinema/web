import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { CssBaseline } from '@mui/material';

import { PrivateRoute } from '../auth/PrivateRoute';
import { AuthLayout } from '../layouts/AuthLayout';
import { MainLayout } from '../layouts/MainLayout';
import { Actors } from '../pages/Actors';
import { Categories } from '../pages/Categories';
import { Dashboard } from '../pages/Dashboard';
import { Movies } from '../pages/Movies';
import { SignIn } from '../pages/SignIn';
import { SignUp } from '../pages/SignUp';
import { GlobalStyle } from '../styles/globalStyles';

export const App = () => {
	const client = new ApolloClient({
		uri: import.meta.env.VITE_BASE_URL,
		cache: new InMemoryCache(),
	});

	return (
		<ApolloProvider client={client}>
			<CssBaseline />
			<GlobalStyle />
			<ToastContainer />
			<BrowserRouter>
				<Routes>
					<Route element={<PrivateRoute />}>
						<Route path="/" element={<MainLayout />}>
							<Route index element={<Dashboard />} />
							<Route path="/actors" element={<Actors />} />
							<Route path="/movies" element={<Movies />} />
							<Route path="/categories" element={<Categories />} />
							<Route path="*" element={<p>There's nothing here: 404!</p>} />
						</Route>
					</Route>
					<Route path="/accounts" element={<AuthLayout />}>
						<Route path="signin" element={<SignIn />} />
						<Route path="signup" element={<SignUp />} />
					</Route>
				</Routes>
			</BrowserRouter>
		</ApolloProvider>
	);
};
