import { create } from 'zustand';

export interface ICategoryStore {
	count: number;
	setCount(_value: number): void;
}

const initStore = {
	count: 0,
};

export const useCategoryStore = create<ICategoryStore>((set) => ({
	...initStore,
	setCount: (val: number) => set(() => ({ count: val })),
}));
