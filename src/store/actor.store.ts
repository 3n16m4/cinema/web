import { create } from 'zustand';

export interface IActorStore {
	count: number;
	setCount(_value: number): void;
}

const initStore = {
	count: 0,
};

export const useActorStore = create<IActorStore>((set) => ({
	...initStore,
	setCount: (val: number) => set(() => ({ count: val })),
}));
