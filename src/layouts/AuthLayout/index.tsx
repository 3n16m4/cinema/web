import { Navigate, Outlet } from 'react-router-dom';

import { Box } from '@mui/material';

import { useAuth } from '../../auth/useAuth';

export const AuthLayout = () => {
	const { isAuthenticated } = useAuth();

	if (isAuthenticated) {
		return <Navigate to="/" replace />;
	}

	return (
		<Box sx={{ display: 'flex' }} maxWidth={'1200px'} mx={'auto'}>
			<Outlet />
		</Box>
	);
};
