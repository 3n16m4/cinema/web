import CategoryIcon from '@mui/icons-material/Category';
import MovieIcon from '@mui/icons-material/Movie';
import SignalCellularAltIcon from '@mui/icons-material/SignalCellularAlt';
import SupervisorAccountIcon from '@mui/icons-material/SupervisorAccount';

export const routes = [
	{
		text: 'Dashboard',
		icon: <SignalCellularAltIcon />,
		path: '/',
	},
	{
		text: 'Actors',
		icon: <SupervisorAccountIcon />,
		path: '/actors',
	},
	{
		text: 'Movies',
		icon: <MovieIcon />,
		path: '/movies',
	},
	{
		text: 'Categories',
		icon: <CategoryIcon />,
		path: '/categories',
	},
];
