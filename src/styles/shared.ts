import styled from 'styled-components';

export const Form__LabelError = styled.span`
	font-size: 12px;
	color: #d32f2f;
`;
