import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
	* {
		box-sizing: border-box;
	}
	ul {
		list-style: none;
	}
	.MuiDataGrid-root .MuiDataGrid-cell:focus-within,
	.MuiDataGrid-columnHeader {
		outline: none !important
	}
`;
