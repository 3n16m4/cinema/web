import { Link } from 'react-router-dom';

import { Typography } from '@mui/material';

export const Copyright = () => {
	return (
		<Typography variant="body2" color="text.secondary" align="center">
			{'Copyright © '}
			<Link color="inherit" to="/">
				Online Movies
			</Link>{' '}
			{new Date().getFullYear()}
			{'.'}
		</Typography>
	);
};
