import { routes } from '@/routes';
import { useNavigate } from 'react-router-dom';

import MailIcon from '@mui/icons-material/Mail';
import { Toolbar } from '@mui/material';
import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';

import { useActorStore } from '../../store/actor.store';
import { useCategoryStore } from '../../store/category.store';
import { Aside__ItemCount } from './styled';

export const Aside = () => {
	const navigate = useNavigate();
	const navigateHandler = (path: string) => {
		navigate(path);
	};
	const countActors = useActorStore((state) => state.count);
	const countCategories = useCategoryStore((state) => state.count);

	const getLabel = (label: string) => {
		if (label.toLowerCase().includes('actors')) {
			return countActors ? `(${countActors})` : null;
		}
		if (label.toLowerCase().includes('categories')) {
			return countCategories ? `(${countCategories})` : null;
		}
	};
	return (
		<div>
			<Toolbar />
			<Divider />
			<List>
				{routes.map((route) => (
					<ListItem key={route.text} disablePadding>
						<ListItemButton onClick={() => navigateHandler(route.path)}>
							<ListItemIcon>{route.icon}</ListItemIcon>
							<ListItemText
								primary={route.text}
								secondary={<Aside__ItemCount>{getLabel(route.text)}</Aside__ItemCount>}
							/>
						</ListItemButton>
					</ListItem>
				))}
			</List>
			<Divider />
			<List>
				{['All mail', 'Trash', 'Spam'].map((text) => (
					<ListItem key={text} disablePadding>
						<ListItemButton>
							<ListItemIcon>
								<MailIcon />
							</ListItemIcon>
							<ListItemText primary={text} />
						</ListItemButton>
					</ListItem>
				))}
			</List>
		</div>
	);
};
