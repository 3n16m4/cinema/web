import { Dispatch, SetStateAction } from 'react';

import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { DatePicker as BasicDatePicker } from '@mui/x-date-pickers/DatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';

type PropsT = {
	label: string;
	value: string | null;
	setValue: Dispatch<SetStateAction<string | null>>;
};
export const DatePicker = ({ label, value, setValue }: PropsT) => {
	return (
		<LocalizationProvider dateAdapter={AdapterMoment}>
			<DemoContainer components={['DatePicker']}>
				<BasicDatePicker
					value={value}
					onChange={(newValue) => {
						setValue(newValue);
					}}
					label={label}
				/>
			</DemoContainer>
		</LocalizationProvider>
	);
};
