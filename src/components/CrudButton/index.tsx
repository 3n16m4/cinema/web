import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import SaveIcon from '@mui/icons-material/Save';
import LoadingButton from '@mui/lab/LoadingButton';

type PropsT = {
	type: 'create' | 'update' | 'delete' | 'primary' | 'save';
	withIcon?: boolean;
	loading?: boolean;
	title: string;
	handleClick?: () => void;
};
export const CrudButton = ({ title, loading, type, handleClick }: PropsT) => {
	let color: 'primary' | 'success' | 'warning' | 'error' = 'primary';
	let icon: JSX.Element | null = null;
	if (type === 'create') {
		color = 'success';
		icon = <AddIcon />;
	} else if (type === 'update') {
		color = 'warning';
		icon = <EditIcon />;
	} else if (type === 'delete') {
		color = 'error';
		icon = <DeleteIcon />;
	} else if (type === 'save') {
		color = 'success';
		icon = <SaveIcon />;
	}

	return (
		<LoadingButton
			style={{ minWidth: '100px' }}
			size="small"
			onClick={handleClick}
			startIcon={icon}
			loading={loading}
			loadingPosition="start"
			color={color}
			variant="contained"
		>
			{title}
		</LoadingButton>
	);
};
