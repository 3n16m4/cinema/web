import CategoryIcon from '@mui/icons-material/Category';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';

export const Widget = () => {
	return (
		<Card sx={{ minWidth: 175, maxWidth: 250 }}>
			<CardContent>
				<Box sx={{ display: 'flex', justifyContent: 'center', mb: 1 }}>
					<CategoryIcon fontSize="large" color="primary" />
				</Box>
				<Typography variant="h2" component="div" align="center">
					120
				</Typography>
				<Typography variant="h5" component="div" align="center">
					Actors
				</Typography>
			</CardContent>
		</Card>
	);
};
