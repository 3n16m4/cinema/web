import styled from 'styled-components';

export const Modal__Wrapper = styled.div`
	position: fixed;
	top: 10%;
	left: 50%;
	transform: translate(-50%, 0);
	max-width: 800px;
	background: #fff;
	padding: 10px;
	border-radius: 5px;
`;
