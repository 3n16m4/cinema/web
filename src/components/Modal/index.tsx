import Modal from '@mui/material/Modal';

import { Modal__Wrapper } from './styled';

type PropsT = {
	children: React.ReactNode;
	closeModal: () => void;
	isOpen: boolean;
};
export const ModalWindow = ({ children, closeModal, isOpen }: PropsT) => {
	return (
		<div>
			<Modal
				open={isOpen}
				onClose={closeModal}
				aria-labelledby="modal-modal-title"
				aria-describedby="modal-modal-description"
			>
				<Modal__Wrapper>{children}</Modal__Wrapper>
			</Modal>
		</div>
	);
};
