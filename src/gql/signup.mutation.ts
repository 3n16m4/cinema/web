import { gql } from '@apollo/client';

export const SIGNUP = gql`
	mutation CreateUser(
		$email: String!
		$password: String!
		$is_subscribed: Boolean!
		$first_name: String!
		$last_name: String!
	) {
		createUser(
			email: $email
			password: $password
			is_subscribed: $is_subscribed
			first_name: $first_name
			last_name: $last_name
		) {
			email
		}
	}
`;
