import { gql } from '@apollo/client';

export const GET_ACTORS = gql`
	query Actors($skip: Int!, $take: Int) {
		actors(skip: $skip, take: $take) {
			count
			data {
				first_name
				last_name
				actor_id
				birthday
				img_url
				last_update
			}
		}
	}
`;
