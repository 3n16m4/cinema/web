import { gql } from '@apollo/client';

export const EDIT_CATEGORY = gql`
	mutation EditCategory($id: ID!, $category: String!) {
		editCategory(id: $id, category: $category) {
			category
			category_id
			last_update
		}
	}
`;
