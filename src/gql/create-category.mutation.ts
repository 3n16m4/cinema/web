import { gql } from '@apollo/client';

export const CREATE_CATEGORY = gql`
	mutation CreateCategory($category: String!) {
		createCategory(category: $category) {
			category
			category_id
			last_update
		}
	}
`;
