import { gql } from '@apollo/client';

export const GET_CATEGORIES = gql`
	query Category {
		categories {
			count
			data {
				category_id
				category
				last_update
			}
		}
	}
`;
