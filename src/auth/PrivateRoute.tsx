import { Navigate, Outlet } from 'react-router-dom';

import { useAuth } from './useAuth';

type PropsT = {
	children?: React.ReactNode;
};

export const PrivateRoute = ({ children }: PropsT) => {
	const { isAuthenticated } = useAuth();
	if (!isAuthenticated) {
		return <Navigate to="/accounts/signin" replace />;
	}
	return children ? children : <Outlet />;
};
