export const useAuth = () => {
	const isAuthenticated = false;
	return {
		isAuthenticated,
	};
};
