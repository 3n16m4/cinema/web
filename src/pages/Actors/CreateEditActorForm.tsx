import { MuiFileInput } from 'mui-file-input';
import { useState } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';

import AttachFileIcon from '@mui/icons-material/AttachFile';
import { Box, Button, TextField, Typography } from '@mui/material';

import { CrudButton } from '../../components/CrudButton';
import { DatePicker } from '../../components/DatePicker';
import { CreateEditActorInputs } from './types';

type PropsT = {
	loading?: boolean;
	type?: 'create' | 'update';
	closeModal: () => void;
	submitFormHandler: (_data: CreateEditActorInputs) => void;
};

export const CreateEditActorForm = ({
	loading,
	closeModal,
	// submitFormHandler,
	type = 'create',
}: PropsT) => {
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<CreateEditActorInputs>();
	const title = type === 'create' ? 'Create' : 'Update';

	const [file, setFile] = useState<File | null>(null);
	const [birthday, setBirthday] = useState<string | null>(null);

	const onSubmit: SubmitHandler<CreateEditActorInputs> = (data) => {
		console.log(data, birthday);

		// submitFormHandler(data)
	};

	const handleChange = (newFile: File | null) => {
		setFile(newFile);
	};

	return (
		<Box width={500} mx="auto">
			<Box mb={2}>
				<Typography variant="h6" align="center">
					{`${title} Actor`}
				</Typography>
			</Box>
			<form onSubmit={handleSubmit(onSubmit)}>
				<Box mb={2}>
					<TextField fullWidth label="First Name" {...register('first_name', { required: true })} />
					{errors.first_name && (
						<Typography color={'error'} variant="caption">
							This field is required
						</Typography>
					)}
				</Box>
				<Box mb={2}>
					<TextField fullWidth label="Last Name" {...register('last_name', { required: true })} />
					{errors.last_name && (
						<Typography color={'error'} variant="caption">
							This field is required
						</Typography>
					)}
				</Box>
				<Box mb={2}>
					<DatePicker label="Date of Birth" value={birthday} setValue={setBirthday} />
				</Box>
				<Box mb={2}>
					<MuiFileInput
						fullWidth
						label="Image"
						size="medium"
						value={file}
						onChange={handleChange}
						variant="outlined"
						InputProps={{
							inputProps: {
								accept: 'image/*',
							},
							startAdornment: <AttachFileIcon />,
						}}
					/>
				</Box>
				<Box justifyContent={'space-between'} display={'flex'}>
					<Button variant="contained" color="error" onClick={closeModal}>
						Close
					</Button>
					<CrudButton
						loading={loading}
						handleClick={handleSubmit(onSubmit)}
						type="save"
						title={title}
					/>
				</Box>
			</form>
		</Box>
	);
};
