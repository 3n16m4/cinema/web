import { useEffect } from 'react';

import { Box } from '@mui/material';

import { CrudButton } from '@/components/CrudButton';
import { ModalWindow } from '@/components/Modal';

import { useActorStore } from '../../store/actor.store';
import { ActorTable } from './ActorTable';
import { CreateEditActorForm } from './CreateEditActorForm';
import { useActor } from './hooks/useActor';

export const Actors = () => {
	const { isOpen, closeModal, openModal, submitFormHandler, actors, count } = useActor();
	const setCount = useActorStore((state) => state.setCount);

	useEffect(() => {
		setCount(count);
	}, [count, setCount]);

	return (
		<Box>
			<Box mb={2} style={{ display: 'flex', justifyContent: 'flex-end' }}>
				<CrudButton type="create" title="Add Actor" handleClick={openModal} />
			</Box>
			{actors && <ActorTable actors={actors} />}
			<ModalWindow isOpen={isOpen} closeModal={closeModal}>
				<CreateEditActorForm
					type="create"
					closeModal={closeModal}
					submitFormHandler={submitFormHandler}
				/>
			</ModalWindow>
		</Box>
	);
};
