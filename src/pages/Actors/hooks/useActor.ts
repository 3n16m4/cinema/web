import { useCallback, useState } from 'react';
import { toast } from 'react-toastify';

import { useActorsQuery, useCreateCategoryMutation } from '@/generated/graphql';

import { CreateEditActorInputs } from '../types';

export const useActor = () => {
	const [createCategoryMutation] = useCreateCategoryMutation();

	const [isOpen, setIsOpen] = useState(false);

	const closeModal = useCallback(() => {
		setIsOpen(false);
	}, [setIsOpen]);

	const openModal = useCallback(() => {
		setIsOpen(true);
	}, [setIsOpen]);

	const submitFormHandler = useCallback(
		async (data: CreateEditActorInputs) => {
			await createCategoryMutation({
				variables: {
					category: data.first_name,
				},
				onError: (error) => {
					toast.error(error.message);
				},
				onCompleted: () => {
					toast.success('Category created successfully');
					closeModal();
				},
				refetchQueries: ['Category'],
			});
		},
		[closeModal, createCategoryMutation],
	);

	const { data } = useActorsQuery({ variables: { skip: 0, take: 20 } });

	const actors = data?.actors?.data;
	return {
		submitFormHandler,
		isOpen,
		closeModal,
		openModal,
		actors,
		count: data?.actors?.count || 0,
	};
};
