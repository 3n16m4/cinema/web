import moment from 'moment';
import { useCallback, useState } from 'react';
import { toast } from 'react-toastify';

import { Avatar } from '@mui/material';
import { GridColDef } from '@mui/x-data-grid';

import { CrudButton } from '@/components/CrudButton';

import { Actor, useDeleteCategoryMutation, useEditCategoryMutation } from '@/generated/graphql';

import { CreateEditActorInputs } from '../types';

type PropsT = {
	actors: Actor[];
};

export const useActorTable = ({ actors }: PropsT) => {
	const [deleteActorId, setDeleteActorId] = useState('');
	const [editActorId, setEditActorId] = useState('');

	const closeEditModal = useCallback(() => {
		setEditActorId('');
	}, [setEditActorId]);

	const closeDeleteModal = useCallback(() => {
		setDeleteActorId('');
	}, [setDeleteActorId]);

	const openEditModal = useCallback(
		(id: string) => {
			setEditActorId(id);
		},
		[setEditActorId],
	);
	const openDeleteModal = useCallback(
		(id: string) => {
			setDeleteActorId(id);
		},
		[setDeleteActorId],
	);
	const [deleteCategoryMutation, { loading: deleteCategoryLoading }] = useDeleteCategoryMutation();
	const [editCategoryMutation, { loading: editCategoryLoading }] = useEditCategoryMutation();

	const editActorHandler = useCallback(
		async (id: string, categoryName: string) => {
			await editCategoryMutation({
				variables: {
					id,
					category: categoryName,
				},
				onError: (error) => {
					toast.error(error.message);
				},
				onCompleted: () => {
					toast.success('Category was updated successfully');
					closeEditModal();
				},
				refetchQueries: ['Category'],
			});
		},
		[closeEditModal, editCategoryMutation],
	);
	const deleteActorHandler = useCallback(
		async (id: string) => {
			await deleteCategoryMutation({
				variables: {
					id,
				},
				onError: (error) => {
					toast.error(error.message);
				},
				onCompleted: () => {
					toast.success('Category was deleted successfully');
					closeDeleteModal();
				},
				refetchQueries: ['Category'],
			});
		},
		[closeDeleteModal, deleteCategoryMutation],
	);

	const removeHandler = () => {
		deleteActorHandler(deleteActorId);
	};
	const editHandler = (data: CreateEditActorInputs) => {
		editActorHandler(editActorId, data.first_name);
	};

	const columns: GridColDef[] = [
		{
			field: 'img_url',
			headerName: 'Avatar',
			width: 80,
			disableColumnMenu: true,
			sortable: false,
			renderCell: (params) => {
				return <Avatar alt="avatar" src={params.value} />;
			},
		},
		{
			field: 'name',
			headerName: 'Name',
			width: 200,
			disableColumnMenu: true,
			sortable: true,
			sortingOrder: ['asc', 'desc'],
		},
		{
			field: 'birthday',
			headerName: 'Birthday',
			width: 160,
			sortable: true,
			sortingOrder: ['asc', 'desc'],
			disableColumnMenu: true,
			valueFormatter: (params) => {
				return moment(new Date(+params.value)).format('YYYY-MM-DD');
			},
		},
		{
			field: 'last_update',
			headerName: 'Updated',
			width: 160,
			sortable: true,
			sortingOrder: ['asc', 'desc'],
			disableColumnMenu: true,
			valueFormatter: (params) => {
				return moment(new Date(+params.value)).format('YYYY-MM-DD: HH:mm:ss');
			},
		},
		{
			field: 'remove',
			headerName: 'Remove',
			width: 120,
			sortable: false,
			disableColumnMenu: true,
			renderCell: (params) => (
				<CrudButton
					type="delete"
					title="Remove"
					handleClick={() => openDeleteModal(params.row.id as string)}
				/>
			),
		},
		{
			field: 'edit',
			headerName: 'Edit',
			width: 120,
			sortable: false,
			disableColumnMenu: true,
			renderCell: (params) => (
				<CrudButton
					type="update"
					title="Edit"
					handleClick={() => openEditModal(params.row.id as string)}
				/>
			),
		},
	];

	const prepareRows = (actors: Actor[]) => {
		return actors.map((c) => ({
			...c,
			id: c.actor_id,
			name: `${c.first_name} ${c.last_name}`,
		}));
	};

	return {
		columns,
		rows: prepareRows(actors),
		deleteActorId,
		closeDeleteModal,
		removeHandler,
		editHandler,
		editActorId,
		closeEditModal,
		loading: deleteCategoryLoading || editCategoryLoading,
	};
};
