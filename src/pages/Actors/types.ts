export type CreateEditActorInputs = {
	first_name: string;
	last_name: string;
	birthday?: string;
	img?: File | null;
};
