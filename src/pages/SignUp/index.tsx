import { SubmitHandler, useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';

import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import Container from '@mui/material/Container';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';

import { Copyright } from '@/components/Copyright';

import { useCreateUserMutation } from '@/generated/graphql';

import { Form__LabelError } from '@/styles/shared';

type Inputs = {
	first_name: string;
	last_name: string;
	password: string;
	email: string;
	is_subscribed: boolean;
};

export const SignUp = () => {
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<Inputs>();
	const [createUserMutation] = useCreateUserMutation();

	const onSubmit: SubmitHandler<Inputs> = async (data) => {
		await createUserMutation({
			variables: {
				...data,
			},
			onError: (error) => {
				toast.error(error.message);
			},
			onCompleted: () => {
				console.log('completed');
			},
		});
	};

	return (
		<Container component="main" maxWidth="xs">
			<Box
				sx={{
					marginTop: 8,
					display: 'flex',
					flexDirection: 'column',
					alignItems: 'center',
				}}
			>
				<Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
					<LockOutlinedIcon />
				</Avatar>
				<Typography component="h1" variant="h5">
					Sign up
				</Typography>
				<Box component="form" noValidate onSubmit={handleSubmit(onSubmit)} sx={{ mt: 3 }}>
					<Grid container spacing={2}>
						<Grid item xs={12} sm={6}>
							<TextField
								autoComplete="given-name"
								fullWidth
								label="First Name"
								autoFocus
								{...register('first_name', { required: true })}
							/>
							{errors.first_name && <Form__LabelError>This field is required</Form__LabelError>}
						</Grid>
						<Grid item xs={12} sm={6}>
							<TextField
								fullWidth
								label="Last Name"
								autoComplete="family-name"
								{...register('last_name', { required: true })}
							/>
							{errors.last_name && <Form__LabelError>This field is required</Form__LabelError>}
						</Grid>
						<Grid item xs={12}>
							<TextField
								fullWidth
								label="Email Address"
								autoComplete="email"
								{...register('email', { required: true })}
							/>
							{errors.email && <Form__LabelError>This field is required</Form__LabelError>}
						</Grid>
						<Grid item xs={12}>
							<TextField
								fullWidth
								label="Password"
								type="password"
								autoComplete="new-password"
								{...register('password', { required: true })}
							/>
							{errors.password && <Form__LabelError>This field is required</Form__LabelError>}
						</Grid>
						<Grid item xs={12}>
							<FormControlLabel
								control={<Checkbox color="primary" {...register('is_subscribed')} />}
								label="I want to receive inspiration, marketing promotions and updates via email."
							/>
						</Grid>
					</Grid>
					<Button type="submit" fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}>
						Sign Up
					</Button>
					<Grid container justifyContent="flex-end" mb={2}>
						<Box sx={{ textAlign: 'center', width: '100%' }} mb={2}>
							<Link to="/accounts/reset">Forgotten your password?</Link>
						</Box>
						<Box sx={{ textAlign: 'center', width: '100%' }} mb={6}>
							<Link to="/accounts/signin">Already have an account? Sign in</Link>
						</Box>
					</Grid>
				</Box>
			</Box>
			<Copyright />
		</Container>
	);
};
