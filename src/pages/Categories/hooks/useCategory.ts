import { useCallback, useState } from 'react';
import { toast } from 'react-toastify';

import { useCategoryQuery, useCreateCategoryMutation } from '@/generated/graphql';

import { CreateEditCategoryInputs } from '../types';

export const useCategory = () => {
	const [createCategoryMutation] = useCreateCategoryMutation();

	const [isOpen, setIsOpen] = useState(false);

	const closeModal = useCallback(() => {
		setIsOpen(false);
	}, [setIsOpen]);

	const openModal = useCallback(() => {
		setIsOpen(true);
	}, [setIsOpen]);

	const submitFormHandler = useCallback(
		async (data: CreateEditCategoryInputs) => {
			await createCategoryMutation({
				variables: {
					category: data.category,
				},
				onError: (error) => {
					toast.error(error.message);
				},
				onCompleted: () => {
					toast.success('Category created successfully');
					closeModal();
				},
				refetchQueries: ['Category'],
			});
		},
		[closeModal, createCategoryMutation],
	);

	const { data } = useCategoryQuery();
	const categories = data?.categories;
	return {
		submitFormHandler,
		isOpen,
		closeModal,
		openModal,
		categories,
		count: data?.categories?.count || 0,
	};
};
