import moment from 'moment';
import { useCallback, useState } from 'react';
import { toast } from 'react-toastify';

import { GridColDef } from '@mui/x-data-grid';

import { CrudButton } from '@/components/CrudButton';

import { Category, useDeleteCategoryMutation, useEditCategoryMutation } from '@/generated/graphql';

import { CreateEditCategoryInputs } from '../types';

type PropsT = {
	categories: Category[];
};

export const useCategoryTable = ({ categories }: PropsT) => {
	const [deleteCategoryId, setDeleteCategoryId] = useState('');
	const [editCategoryId, setEditCategoryId] = useState('');

	const closeEditModal = useCallback(() => {
		setEditCategoryId('');
	}, [setEditCategoryId]);

	const closeDeleteModal = useCallback(() => {
		setDeleteCategoryId('');
	}, [setDeleteCategoryId]);

	const openEditModal = useCallback(
		(id: string) => {
			setEditCategoryId(id);
		},
		[setEditCategoryId],
	);
	const openDeleteModal = useCallback(
		(id: string) => {
			setDeleteCategoryId(id);
		},
		[setDeleteCategoryId],
	);
	const [deleteCategoryMutation, { loading: deleteCategoryLoading }] = useDeleteCategoryMutation();
	const [editCategoryMutation, { loading: editCategoryLoading }] = useEditCategoryMutation();

	const editCategoryHandler = useCallback(
		async (id: string, categoryName: string) => {
			await editCategoryMutation({
				variables: {
					id,
					category: categoryName,
				},
				onError: (error) => {
					toast.error(error.message);
				},
				onCompleted: () => {
					toast.success('Category was updated successfully');
					closeEditModal();
				},
				refetchQueries: ['Category'],
			});
		},
		[closeEditModal, editCategoryMutation],
	);
	const deleteCategoryHandler = useCallback(
		async (id: string) => {
			await deleteCategoryMutation({
				variables: {
					id,
				},
				onError: (error) => {
					toast.error(error.message);
				},
				onCompleted: () => {
					toast.success('Category was deleted successfully');
					closeDeleteModal();
				},
				refetchQueries: ['Category'],
			});
		},
		[closeDeleteModal, deleteCategoryMutation],
	);

	const removeHandler = () => {
		deleteCategoryHandler(deleteCategoryId);
	};
	const editHandler = (data: CreateEditCategoryInputs) => {
		editCategoryHandler(editCategoryId, data.category);
	};

	const columns: GridColDef[] = [
		{
			field: 'category',
			headerName: 'Category',
			width: 140,
			disableColumnMenu: true,
			sortable: true,
			sortingOrder: ['asc', 'desc'],
		},
		{
			field: 'last_update',
			headerName: 'Updated',
			width: 160,
			sortable: true,
			sortingOrder: ['asc', 'desc'],
			disableColumnMenu: true,
			valueFormatter: (params) => {
				return moment(new Date(+params.value)).format('YYYY-MM-DD: HH:mm:ss');
			},
		},
		{
			field: 'remove',
			headerName: 'Remove',
			width: 120,
			sortable: false,
			disableColumnMenu: true,
			renderCell: (params) => (
				<CrudButton
					type="delete"
					title="Remove"
					handleClick={() => openDeleteModal(params.row.id as string)}
				/>
			),
		},
		{
			field: 'edit',
			headerName: 'Edit',
			width: 120,
			sortable: false,
			disableColumnMenu: true,
			renderCell: (params) => (
				<CrudButton
					type="update"
					title="Edit"
					handleClick={() => openEditModal(params.row.id as string)}
				/>
			),
		},
	];

	const prepareRows = (categories: Category[]) => {
		return categories.map((c) => ({
			...c,
			id: c.category_id,
		}));
	};

	return {
		columns,
		rows: prepareRows(categories),
		deleteCategoryId,
		closeDeleteModal,
		removeHandler,
		editHandler,
		editCategoryId,
		closeEditModal,
		loading: deleteCategoryLoading || editCategoryLoading,
	};
};
