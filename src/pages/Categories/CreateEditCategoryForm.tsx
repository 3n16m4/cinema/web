import { SubmitHandler, useForm } from 'react-hook-form';

import { Box, Button, TextField, Typography } from '@mui/material';

import { CrudButton } from '../../components/CrudButton';
import { CreateEditCategoryInputs } from './types';

type PropsT = {
	loading?: boolean;
	type?: 'create' | 'update';
	categoryName?: string;
	closeModal: () => void;
	submitFormHandler: (_data: CreateEditCategoryInputs) => void;
};

export const CreateEditCategoryForm = ({
	loading,
	closeModal,
	submitFormHandler,
	categoryName,
	type = 'create',
}: PropsT) => {
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<CreateEditCategoryInputs>();
	const onSubmit: SubmitHandler<CreateEditCategoryInputs> = (data) => submitFormHandler(data);
	const title = type === 'create' ? 'Create' : 'Update';
	return (
		<Box width={500} mx="auto">
			<Box mb={2}>
				<Typography variant="h6" align="center">
					{`${title} Category`}
				</Typography>
			</Box>
			<form onSubmit={handleSubmit(onSubmit)}>
				<Box mb={2}>
					<TextField
						defaultValue={categoryName}
						fullWidth
						label="Category"
						{...register('category', { required: true })}
					/>
					{errors.category && (
						<Typography color={'error'} variant="caption">
							This field is required
						</Typography>
					)}
				</Box>
				<Box justifyContent={'space-between'} display={'flex'}>
					<Button variant="contained" color="error" onClick={closeModal}>
						Close
					</Button>
					<CrudButton
						loading={loading}
						handleClick={handleSubmit(onSubmit)}
						withIcon={false}
						type="save"
						title={title}
					/>
				</Box>
			</form>
		</Box>
	);
};
