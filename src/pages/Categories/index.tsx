import { useEffect } from 'react';

import { Box } from '@mui/material';

import { CrudButton } from '@/components/CrudButton';
import { ModalWindow } from '@/components/Modal';

import { useCategoryStore } from '../../store/category.store';
import { CategoryTable } from './CategoryTable';
import { CreateEditCategoryForm } from './CreateEditCategoryForm';
import { useCategory } from './hooks/useCategory';

export const Categories = () => {
	const { isOpen, closeModal, openModal, submitFormHandler, categories, count } = useCategory();

	const setCount = useCategoryStore((state) => state.setCount);

	useEffect(() => {
		setCount(count);
	}, [count, setCount]);

	return (
		<Box>
			<Box mb={2} style={{ display: 'flex', justifyContent: 'flex-end' }}>
				<CrudButton type="create" title="Add Category" handleClick={openModal} />
			</Box>
			{categories && <CategoryTable categories={categories.data} />}
			<ModalWindow isOpen={isOpen} closeModal={closeModal}>
				<CreateEditCategoryForm
					type="create"
					closeModal={closeModal}
					submitFormHandler={submitFormHandler}
				/>
			</ModalWindow>
		</Box>
	);
};
