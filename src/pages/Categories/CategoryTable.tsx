import { Box, Button, Typography } from '@mui/material';
import { DataGrid } from '@mui/x-data-grid';

import { ModalWindow } from '@/components/Modal';

import { Category } from '@/generated/graphql';

import { CreateEditCategoryForm } from './CreateEditCategoryForm';
import { useCategoryTable } from './hooks/useCategoryTable';

type PropsT = {
	categories: Category[];
};

export const CategoryTable = ({ categories }: PropsT) => {
	const {
		loading,
		columns,
		rows,
		deleteCategoryId,
		closeDeleteModal,
		removeHandler,
		editHandler,
		editCategoryId,
		closeEditModal,
	} = useCategoryTable({ categories });

	return (
		<Box style={{ width: '100%' }}>
			<DataGrid
				rows={rows}
				columns={columns}
				pagination
				rowSelection={false}
				initialState={{
					pagination: {
						paginationModel: { page: 0, pageSize: 50 },
					},
				}}
				pageSizeOptions={[10, 20, 50, 100, 150]}
			/>
			<ModalWindow isOpen={!!deleteCategoryId} closeModal={closeDeleteModal}>
				<Box mb={2}>
					<Typography variant="h5" align="center">
						Delete Category
					</Typography>
				</Box>
				<Box mb={2}>
					<Typography variant="body1">Are you sure you want to delete this category?</Typography>
					<Typography variant="body1">This action cannot be undone.</Typography>
				</Box>
				<Box justifyContent={'space-between'} display={'flex'}>
					<Button variant="contained" onClick={closeDeleteModal}>
						Cancel
					</Button>
					<Button color="error" variant="contained" onClick={removeHandler}>
						Delete
					</Button>
				</Box>
			</ModalWindow>
			<ModalWindow isOpen={!!editCategoryId} closeModal={closeEditModal}>
				<CreateEditCategoryForm
					categoryName={
						categories.find((category) => category.category_id === editCategoryId)?.category
					}
					type="update"
					closeModal={closeEditModal}
					submitFormHandler={editHandler}
					loading={loading}
				/>
			</ModalWindow>
		</Box>
	);
};
