import { Box } from '@mui/material';

import { Widget } from '@/components/Widget';

export const Dashboard = () => {
	return (
		<Box>
			<Box>
				<Widget />
			</Box>
		</Box>
	);
};
