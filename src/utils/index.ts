import { routes } from '@/routes';

export const getHeaderTitle = (path: string) => {
	return routes.find((route) => route.path === path)?.text;
};
